﻿MathSoc Logo Usage Guidelines
=============================

Colour Variants
-----------------------------
There are several colour variants of the logo to suit different use cases:

- Standard Variants
	- Use the CMYK variants in COLOUR printing
	- Use the RGB variants for WEB and DIGITAL media.
- Inverse Variants
	- Use the standard logo on all backgrounds, light and dark, that are NOT Math Pink.
	- Use inverse variants ONLY on Math Pink backgrounds.
- Monochrome Variants
	- Use the K variantd for BLACK & WHITE printing ONLY (this should include black & white distributed PDFs)

In all cases, you should avoid backgrounds whose colours are close to, but not-quite Math Pink, since they'll make the standard logo difficult to discern, and dilutes the branding of the Math Faculty when used with the inverse logo. If you WANT to use a pink backdrop, use a shade of MATH PINK, the colour values for which are as follows for web and print:

┌──────────────────────┬─────────────────┬─────────────────┐
│ LOGO VARIANT         │ Standard        │ Inverse         │
├──────────────────────┼─────────────────┼─────────────────┤
│ RGB (Web/Screen)     │ 255, 190, 239   │ 198, 0, 120     │
├──────────────────────┼─────────────────┼─────────────────┤
│ RGB HEX (Web/Screen) │ #FF63AA         │ #C60078         │
├──────────────────────┼─────────────────┼─────────────────┤
│ CMYK (Colour Print)  │ 3%, 29%, 0%, 0% │ 5%, 90%, 0%, 0% │
└──────────────────────┴─────────────────┴─────────────────┘

Note that the CMYK colours are actually slightly DIFFERENT from the RGB ones. As such you should **NOT** use the CMYK values for web/screen media, nor the RGB values for colour printing.

For more information regarding UWaterloo's colour usage for the Mathematics faculty, see https://uwaterloo.ca/brand/visual-expression/colour-palette#Math.

The MathSoc Wordmark
------------------------

The MathSoc wordmark is set in Gotham Bold, to match the rest of the school's branding. When reproducing the wordmark, always ensure:

- The wordmark appears in all caps, as 'MATHSOC'.
- The wordmark is set in Gotham Bold.
	- The 'MATH' portion is set in Gotham's BOOK weight.
	- The 'SOC' portion is set in Gotham's BOLD weight.
	- Both 'MATH' and 'SOC' are the same colour as the base of the logo, and ar standard tracking.

When using the wordmark alongside the logo, use one of the provided assets to ensure that the wordmark and logo are sized and spaced appropriately relative to each other. In cases where this is impossible, and the wordmark and logo must be arranged manually, follow the clearspace guidelines below.

Clear Space
-----------------------------

When using the logo alone, make sure you give it 30% clear space on all sides. For example, if you make the logo 10cm tall, there should be at least 3cm of padding around the logo on all sides, meaning you'd need a 16cm square or larger for the logo:

┌──────────────────────────────────┐
│                                  │
│               ≥30%               │
│                                  │
│                ╱╲                │
│              ╱    ╲              │
│            ╱        ╲            │
│          ╱            ╲          │
│        ╱      100%      ╲  ≥30%  │
│  ≥30%  ╲                ╱        │
│          ╲            ╱          │
│            ╲        ╱            │
│              ╲    ╱              │
│                ╲╱                │
│                                  │
│               ≥30%               │
│                                  │
└──────────────────────────────────┘

When using it HORIZONTALLY alongside the MathSoc wordmark, *always* place the wordmark to the RIGHT of the logo, and leave 20% horizontal clearspace between the logo and the wordmark:

┌──────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                                                              │
│               ≥30%                                                                           │
│                                                                                              │
│                ╱╲                                                                            │
│              ╱    ╲                                                                          │
│            ╱        ╲          M     M     A   TTTTTTT H     H  SSSSS   OOO     CCC  - - - - │ - =
│          ╱            ╲        MM   MM    A A     T    H     H S       O   O   C   C         │   |
│        ╱      100%      ╲      M M M M   A   A    T    H     H S      O     O C              │   |
│  ≥30%  ╲                ╱ 25%  M  M  M  AAAAAAA   T    HHHHHHH  SSSS  O     O C        ≥30%  │  50%
│          ╲            ╱        M     M A       A  T    H     H      S  O   O   C   C         │   |
│            ╲        ╱          M     M A       A  T    H     H SSSSS    OOO     CCC  - - - - │ - =
│              ╲    ╱                                                                          │
│                ╲╱                                                                            │
│                                                                                              │
│               ≥30%                                                                           │
│                                                                                              │
└──────────────────────────────────────────────────────────────────────────────────────────────┘

When using it VERTICALLY alongside the MathSoc wordmark, *always* place the wordmark BELOW the logo, leave 10% vertical clearspace between the logo and the wordmark, and 30% horizontal space bet:

┌──────────────────────────────────────────┐
│                                          │
│                   ≥30%                   │
│                                          │
│                    ╱╲                    │
│                  ╱    ╲                  │
│                ╱        ╲                │
│              ╱            ╲              │
│            ╱      100%      ╲            │
│            ╲                ╱            │
│              ╲            ╱              │
│                ╲        ╱                │
│                  ╲    ╱                  │
│                    ╲╱                    │
│                                          │
│                    15%                   │
│                                          │
│    ≥30%   M  A  T  H   S  O  C   ≥30%    │
│           |                  |           │
│                   ≥30%                   │
│           |                  |           │
└──────────────────────────────────────────┘
            |                  |
            ||----- 150% -----||

### 